<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    protected $attributes = array(
        'thread_status' => 1
    );
    protected $table = 'tbl_thread';
    protected $primaryKey = 'thread_id';
    public $timestamps = false;

}
