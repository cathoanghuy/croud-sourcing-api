<?php
/**
 * Created by PhpStorm.
 * User: hchoang
 * Date: 8/30/2016
 * Time: 6:41 AM
 */

namespace App\Exceptions;


class ApiException extends \Exception
{
    protected $code;
    public function __construct($message, $code)
    {
        $this->code = $code;
        parent::__construct($message);
    }
}