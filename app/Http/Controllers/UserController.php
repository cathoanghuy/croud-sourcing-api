<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function profile(Request $request)
    {
        $response = $this->getResponse();

        $response->setData("user", Auth::user());

        return response()->json($response->getData());
    }


    public function register(Request $request)
    {
        $response = $this->getResponse();
        $user = new User();
        try
        {
            $newUser = $request->get('new_user');
            if (!$newUser) {
                throw new ApiException("Missing argument", "403");
            }
            $users = User::where('reviewer_email', $newUser['reviewer_email'])->first();
            if (!is_null($users)) {
                throw new ApiException("Email address already used", "403");
            }
            foreach ($newUser as $key => $value)
            {
                if ($key == "reviewer_password") {
                    $value = md5($value);
                }
                $user->$key = $value;
            }
            $user->save();
            $response->setMessage('Register successfully');
        } catch (ApiException $ex) {
            $response->setMessage($ex->getMessage());
            $response->setCode($ex->getCode());
        }

        return response()->json($response->getData());
    }

    public function logout(Request $request)
    {
        $response = $this->getResponse();
        try {
            $token = $request->get('api_token');
            /** @var User $user */
            $user = User::where('api_token', $token)->firstOrFail();

            $user->api_token = null;

            $user->save();
            $response->setMessage('Logout Successfully');

        } catch (\Exception $ex) {

        }
        return response()->json($response->getData());
    }

    public function update(Request $request)
    {
        $response = $this->getResponse();
        /** @var User $user */
        $user = Auth::user();
        try
        {
            $update = $request->get('update_detail');
            foreach ($update as $key => $value)
            {
                if ($key == "reviewer_password") {
                    $value = md5($value);
                }
                $user->$key = $value;
            }
            $user->save();
            $response->setMessage("User info updated");
        } catch (\Exception $ex) {

        }
        return response()->json($response->getData());
    }

    public function login(Request $request)
    {
        $response = $this->getResponse();
        $email = $request->input('reviewer_email');
        $password = md5($request->input('reviewer_password'));
        try
        {
            /** @var User $user */
            $user = User::where('reviewer_email', $email)
                ->where('reviewer_password', $password)
                ->firstOrFail();

            $token = null;

            if ($user->reviewer_status == 0) {
                throw new ApiException("Your account is disable, please contact administrator for more information", 403);
            }

            if (!$user->api_token) {
                $token = User::generateToken();
                $user->api_token = $token;
                $user->save();
            }

            $response->setMessage("Login Successfully");
            $response->setData("api_token", $user->api_token);
            $response->setData("user", $user);
            return response()->json($response->getData());

        } catch (ModelNotFoundException $exception) {
            $response->setCode($exception->getCode());
            $response->setMessage("Login details are incorrect");
        } catch (ApiException $exception) {
            $response->setCode($exception->getCode());
            $response->setMessage($exception->getMessage());
        }
        return response()->json($response->getData());
    }
}
