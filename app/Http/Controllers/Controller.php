<?php

namespace App\Http\Controllers;

use App\Response\ApiResponse;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $response;

    /**
     * @return ApiResponse
     */
    public function getResponse()
    {
        if (!$this->response) {
            $this->response = new ApiResponse();
        }

        return $this->response;
    }
}
