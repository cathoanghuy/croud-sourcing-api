<?php

namespace App\Http\Controllers;

use App\Category;
use App\Service;
use Illuminate\Http\Request;

use App\Http\Requests;

class CategoryController extends Controller
{
    public function listCategory(Request $request)
    {
        $response  = $this->getResponse();
        $categories = Category::all();

        $response->setData('categories', $categories);

        return response()->json($response->getData());
    }

    public function explore(Request $request)
    {
        $response = $this->getResponse();
        $categories = Category::all();
        /** @var Category $category */
        foreach ($categories as $category) {
            $services = $category->services()->inRandomOrder()->take(4)->get();
            $category['services'] = $services;
        }

        $response->setData('categories', $categories);
        return response()->json($response->getData());
    }
}
