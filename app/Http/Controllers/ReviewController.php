<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Review;
use App\Service;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    public function listReview(Request $request, $id)
    {
        $response = $this->getResponse();
        /** @var Service $service */
        $service = Service::find($id);
        /** @var User $user */
        $user = Auth::user();

        $reviews = Review::where('review_serv_id', $id)->where('review_reviewer', $user->reviewer_id)->first();

        $service["is_reviewable"] = is_null($reviews);

        $reviews = $service->reviews()->get();

        /** @var Review $review */
        foreach ($reviews as $review) {
            $user = User::find($review["review_reviewer"]);
            $review["author"] = $user;
        }
        $response->setData("service", $service);
        $response->setData("reviews", $reviews);

        return response()->json($response->getData());
    }

    public function postReview(Request $request)
    {
        $response = $this->getResponse();
        $date = new \DateTime();
        $review = new Review();

        try {
            $newReview = $request->get('new_review');
            if (!$newReview) {
                throw new ApiException("Missing attribute", 403);
            }
            foreach ($newReview as $key => $value)
            {
                $review->$key = $value;
            }
            $review->review_date = $date->format("Y-m-d");
            $review->review_time = $date->format("H:i:s");
            $id = $review->review_serv_id;
            $review->save();
            /** @var Service $service */
            $service = Service::find($id);
            $reviews = $service->reviews()->get();
            /** @var Review $review */
            $total = 0;
            foreach ($reviews as $review) {
                $total += $review->getAverageRating();
            }
            $service->editor_ratings = $total/count($reviews);
            $service->save();
            $response->setMessage("Post Review Successfully");
        } catch (ApiException $ex) {
            $response->setMessage($ex->getMessage());
            $response->setCode($ex->getCode());
        }

        return response()->json($response->getData());
    }

    public function editReview(Request $request)
    {

    }

    private function getSpamList(Service $service)
    {
        $total = 0;
        if ($service->reviews()->count() <  10) {
            return;
        }
        $reviews = $service->reviews()->get();
        /** @var Review $review */
        foreach ($reviews as $review) {
            $total += ($review->getAverageRating() - $service->editor_ratings) * ($review->getAverageRating() - $service->editor_ratings);
        }

        $standardDeviation = sqrt($total/count($reviews));

        $standardError = $standardDeviation/sqrt(count($reviews));

//        var_dump($standardError);die;

        $lowerBound = $service->editor_ratings - (1.96 * $standardError);
        $upperBound = $service->editor_ratings + (1.96 * $standardError);
        /** @var Review $review */
        foreach ($reviews as $review) {
            if ($review->getAverageRating() < $lowerBound || $review->getAverageRating() > $upperBound) {
                $review->review_status = 2;
            } else {
                $review->review_status = 1;
            }
            $review->save();
        }
    }

    public function postScanningReview(Request $request)
    {
        $response = $this->getResponse();
        $services = Service::all();
        foreach ($services as $service) {
            $this->getSpamList($service);
        }
        $reviews = Review::where('review_status', 2)->get();
        $response->setData("reviews", $reviews);
        return response()->json($response->getData());
    }
}
