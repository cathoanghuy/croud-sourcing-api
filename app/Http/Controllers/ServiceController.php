<?php

namespace App\Http\Controllers;

use App\Category;
use App\Exceptions\ApiException;
use App\Service;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class ServiceController extends Controller
{
    public function listService(Request $request)
    {
        $response = $this->getResponse();

        $services = Service::select('serv_id', 'serv_name', 'editor_ratings')->orderBy('serv_name')->get();
        $response->setData("services", $services);

        return response()->json($response->getData());
    }

    public function listServiceInCategory(Request $request, $id)
    {
        $response = $this->getResponse();

        /** @var Category $category */
        $category = Category::find($id);
        $response->setData("category", $category);
        $response->setData("services", $category->services()->orderBy('serv_name')->get());

        return response()->json($response->getData());
    }

    public function detailService(Request $request, $id)
    {
        $response = $this->getResponse();
        try {
            /** @var Service $service */
            $service = Service::find($id);
            /** @var User $user */
            $user = Auth::user();
            $reviews = $service->reviews()->where('review_reviewer', $user->id)->first();
            $service["is_reviewable"] = is_null($reviews);

            $response->setData('service', $service);
        } catch (ApiException $exception) {
            $response->setCode($exception->getCode());
            $response->setMessage($exception->getMessage());
        }

        return response()->json($response->getData());
    }

    public function listRandomService(Request $request)
    {
        $response = $this->getResponse();

        $services = Service::inRandomOrder()->take(5)->get();
        $response->setData("services", $services);

        return response()->json($response->getData());
    }
}
