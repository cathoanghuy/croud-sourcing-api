<?php

namespace App\Http\Controllers;

use App\Discussion;
use App\Exceptions\ApiException;
use App\Thread;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class ThreadController extends Controller
{
    public function listThread(Request $request, $id)
    {
        $response = $this->getResponse();
        /** @var Discussion $discussion */
        $discussion = Discussion::find($id);

        $threads = $discussion->threads()->orderBy("thread_date", "DESC")->orderBy("thread_time", "DESC")->get();
        /** @var Thread $thread */
        foreach ($threads as $thread) {
            $thread["author"] = User::find($thread["thread_author"]);
        }

        $response->setData('discussion', $discussion);
        $response->setData('threads', $threads);

        return response()->json($response->getData());
    }

    public function postThread(Request $request, $id)
    {
        $response = $this->getResponse();
        $thread = new Thread();
        $date = new \DateTime();
        try
        {
            $newUser = $request->get('new_thread');
            if (!$newUser) {
                throw new ApiException("Missing attribute", 403);
            }
            foreach ($newUser as $key => $value)
            {
                $thread->$key = $value;
            }
            $thread->thread_date = $date->format("Y-m-d");
            $thread->thread_time = $date->format("H:i:s");
            $thread->save();
            $response->setMessage("Post new thread successfully");
        } catch (ApiException $ex) {
            $response->setCode($ex->getCode());
            $response->setMessage($ex->getMessage());
        }

        return response()->json($response->getData());

    }

    public function editThread(Request $request)
    {

    }
}
