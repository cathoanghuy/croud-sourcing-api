<?php

namespace App\Http\Controllers;

use App\Discussion;
use App\Exceptions\ApiException;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class DiscussionController extends Controller
{
    public function listDiscussion(Request $request)
    {
        $response = $this->getResponse();
        $discussions = Discussion::orderBy("discussion_date", "DESC")->orderBy("discussion_time", "DESC")->get();

        /** @var Discussion $discussion */
        foreach ($discussions as $discussion) {
            $discussion["author"] = User::find($discussion["author_id"]);
        }

        $response->setData('discussions', $discussions);

        return response()->json($response->getData());
    }

    public function newDiscussion(Request $request)
    {
        $response = $this->getResponse();
        $discussion = new Discussion();
        $date = new \DateTime();
        try
        {
            $newUser = $request->get('new_discussion');
            if (!$newUser) {
                throw new ApiException("Missing Argument", 403);
            }
            foreach ($newUser as $key => $value)
            {
                $discussion->$key = $value;
            }
            $discussion->discussion_date = $date->format("Y-m-d");
            $discussion->discussion_time = $date->format("H:i:s");
            $discussion->save();
            $response->setMessage("Post new discussion successfully");
        } catch (ApiException $ex) {
            $response->setMessage($ex->getMessage());
            $response->setCode($ex->getCode());
        }

        return response()->json($response->getData());
    }

    public function deleteDiscussion(Request $request)
    {

    }

    public function editDiscussion(Request $request)
    {

    }
}
