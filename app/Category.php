<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'tbl_category';
    protected $primaryKey = 'cat_id';
    public $timestamps = false;

    public function services()
    {
        return $this->belongsToMany('App\Service', 'tbl_service_category', 'category_id_FRK', 'service_id_FRK');
    }
}
