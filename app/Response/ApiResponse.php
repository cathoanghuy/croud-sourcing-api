<?php
/**
 * Created by PhpStorm.
 * User: hchoang
 * Date: 8/30/2016
 * Time: 6:53 AM
 */

namespace App\Response;


use Illuminate\Http\Response;

class ApiResponse extends Response
{
    private $data = array();

    public function __construct()
    {
        $this->content['code'] = 200;
        $this->data['code'] = 200;
        parent::__construct();
    }

    public function setMessage($message)
    {
        $this->data['message'] = $message;
    }

    public function setCode($code)
    {
        $this->data['code'] = $code;
    }

    public function setData($name, $data)
    {
        $this->data[$name] = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}