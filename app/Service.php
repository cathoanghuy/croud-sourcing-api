<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'tbl_services';
    protected $primaryKey = 'serv_id';
    public $timestamps = false;

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'tbl_service_category', 'service_id_FRK', 'category_id_FRK');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review', 'review_serv_id');
    }
}
