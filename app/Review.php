<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $attributes = array(
        'review_status' => 1
    );
    protected $table = 'tbl_review';
    protected $primaryKey = 'review_id';
    public $timestamps = false;

    public function getAverageRating()
    {
        return ($this->review_ratings + $this->review_value_for_money + $this->review_ease_of_use + $this->review_feature +
		    $this->review_customer_support) / 5;
    }
}
