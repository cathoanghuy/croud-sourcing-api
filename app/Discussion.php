<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{
    protected $attributes = array(
        'discussion_status' => 1
    );

    protected $table = 'tbl_discussion';
    protected $primaryKey = 'discussion_id';
    public $timestamps = false;

    public function threads()
    {
        return $this->hasMany('App\Thread', 'thread_discussion_id');
    }
}
