<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'tbl_reviewer';
    protected $primaryKey = 'reviewer_id';

    protected $attributes = array(
        'reviewer_no_of_reviews' => 0,
        'reviewer_points' => 0,
        'reviewer_status' => 1
    );

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reviewer_first_name', 'reviewer_last_name', 'reviewer_password', 'reviewer_email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'reviewer_password', 'api_token',
    ];

    public $timestamps = false;

    public static function generateToken()
    {
        return str_random(60);
    }
}
