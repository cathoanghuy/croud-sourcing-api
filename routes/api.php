<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'UserController@login');
Route::post('/register', 'UserController@register');

Route::group(['middleware' => 'auth:api'], function () {
    //User profile and update api
    Route::get('/user', 'UserController@profile');
    Route::post('/user/update', 'UserController@update');
    Route::post('/logout', 'UserController@logout');

    //Category
    Route::get('/category', 'CategoryController@listCategory');
    Route::get('/explore', 'CategoryController@explore');

    //Service
    Route::get('/service', 'ServiceController@listService');
    Route::get('/category/{id}/service', 'ServiceController@listServiceInCategory');
    Route::get('/random/service', 'ServiceController@listRandomService');
    Route::get('/service/{id}', 'ServiceController@detailService');

    //Discussion
    Route::get('/discussion', 'DiscussionController@listDiscussion');
    Route::post('/discussion/new', 'DiscussionController@newDiscussion');

    //Thread
    Route::post('discussion/{id}/new', 'ThreadController@postThread');
    Route::get('discussion/{id}', 'ThreadController@listThread');

    //Review
    Route::get('service/{id}/review', 'ReviewController@listReview');
    Route::post('review/new', 'ReviewController@postReview');
    Route::get('review/spams', 'ReviewController@postScanningReview');

});
